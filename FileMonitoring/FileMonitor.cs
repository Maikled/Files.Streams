﻿using System;
using System.IO;
using FileMonitoring.Interfaces;
using System.Text.RegularExpressions;

namespace FileMonitoring
{
	public class FileMonitor : IFileMonitor
	{
		private readonly IConfiguration _configuration;
		private FileSystemWatcher _watcher;
		public FileMonitor(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public void Start()
		{
			_watcher = new FileSystemWatcher(_configuration.Path);
			_watcher.Filter = "*.txt";
			_watcher.Changed += OnChanged;
			_watcher.EnableRaisingEvents = true;

			Directory.CreateDirectory(_configuration.BackupPath);

			foreach (var file in Directory.GetFiles(_configuration.Path, "*.txt"))
            {
				CopyFileWithTimeLastWrite(file);
            }
		}

		public void Stop()
		{
			Directory.Delete(_configuration.BackupPath, true);
			_watcher.Dispose();
		}

		public void Reset(DateTime onDateTime)
		{
			foreach (var file in Directory.GetFiles(_configuration.Path, "*.txt"))
			{
				var files = Directory.GetFiles(_configuration.BackupPath, "*.txt");
				for (int i = files.Length - 1; i >= 0; i--)
				{
					if (File.GetCreationTime(files[i]) <= onDateTime && Path.GetFileName(file).Split('.')[0] == Path.GetFileName(files[i]).Split('-')[0])
					{
						File.Replace(Path.GetFullPath(files[i]), Path.GetFullPath(file), null);
						break;
					}
				}
			}
		}

		public void Dispose()
		{
			Stop();
		}

		private void OnChanged(object source, FileSystemEventArgs e)
        {
			CopyFileWithTimeLastWrite(e.FullPath);
        }

		private void CopyFileWithTimeLastWrite(string fileFullPath)
        {
			var fileDate = Regex.Replace(File.GetLastWriteTime(fileFullPath).ToString(), @"[^\d]+", "");
			var newFileName = Path.GetFileNameWithoutExtension(fileFullPath) + "-" + fileDate + ".txt";
			File.Copy(fileFullPath, Path.Combine(_configuration.BackupPath, newFileName), true);
        }
	}
}
